<?php
return array(
    'name' => '用户随机头像',
    'user' => 'krabs',
    'icon' => 'drupal',
    'mess' => '用户随机头像，采用lincanbin在github开源的Material-Design-Avatars支持库制作，在用户注册账号后取用户名第一位生成头像，此插件需要开启PHP GD 绘图库'
);